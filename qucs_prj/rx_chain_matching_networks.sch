<Qucs Schematic 0.0.20>
<Properties>
  <View=-312,-443,2808,2118,0.909101,455,1041>
  <Grid=10,10,1>
  <DataSet=Rx_chain_matching networks.dat>
  <DataDisplay=Rx_chain_matching networks.dpl>
  <OpenDisplay=1>
  <Script=Rx_chain_matching networks.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <RFEDD TQP3M9036 1 500 180 -26 -40 0 0 "S" 0 "2" 0 "open" 0 "0.2785-j0.1647" 0 "0.0219+j0.02229" 0 "-9.74279+j14.39" 0 "0.2285+j0.07293" 0>
  <GND *7 5 550 300 0 0 0 0>
  <L L5 1 550 270 5 -20 0 1 "61.5 nH" 1 "" 0>
  <GND *2 5 650 260 0 0 0 0>
  <Pac P2 1 650 230 -13 -102 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <C C5 1 590 180 -26 17 0 0 "20.5 pF" 1 "" 0 "neutral" 0>
  <GND *6 5 450 300 0 0 0 0>
  <C C4 1 450 270 15 -20 0 1 "2.36 pF" 1 "" 0 "neutral" 0>
  <L L4 1 400 180 -26 10 0 0 "16.7 nH" 1 "" 0>
  <Pac P1 1 320 210 -26 -83 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 5 320 290 0 0 0 0>
  <GND *9 5 530 570 0 0 0 0>
  <L L6 1 530 540 5 -20 0 1 "68 nH" 1 "" 0>
  <RFEDD TQP3M9037 1 480 450 -26 -40 0 0 "S" 0 "2" 0 "open" 0 "0.2785-j0.1647" 0 "0.0219+j0.02229" 0 "-9.74279+j14.39" 0 "0.2285+j0.07293" 0>
  <C C8 1 390 450 -20 -53 0 2 "100 pF" 1 "" 0 "neutral" 0>
  <Pac P3 1 330 480 -38 -84 0 1 "3" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *11 5 330 560 0 0 0 0>
  <GND *8 5 620 530 0 0 0 0>
  <Pac P4 1 620 500 -18 -105 0 1 "4" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <RFEDD MAX2637 1 1700 180 -26 -40 0 0 "S" 0 "2" 0 "open" 0 "0.60408-j0.55918" 0 "0.0015072254+j0.0043615107" 0 "-2.8976231+j1.7137047" 0 "0.35996348-j0.17511106" 0>
  <GND *14 5 1770 560 0 0 0 0>
  <L L8 1 1820 450 -24 -42 0 2 "20 nH" 1 "" 0>
  <C C9 1 1770 510 17 -26 0 1 "2 pF" 1 "" 0 "neutral" 0>
  <RFEDD MAX2634 1 1720 450 -26 -40 0 0 "S" 0 "2" 0 "open" 0 "0.60408-j0.55918" 0 "0.0015072254+j0.0043615107" 0 "-2.8976231+j1.7137047" 0 "0.35996348-j0.17511106" 0>
  <L L7 1 1630 450 -24 -47 0 2 "56 nH" 1 "" 0>
  <Pac P7 1 1500 480 -32 -86 0 1 "7" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *22 5 1500 560 0 0 0 0>
  <L L10 1 1590 180 -26 10 0 0 "56.7 nH" 1 "" 0>
  <GND *17 5 1650 300 0 0 0 0>
  <L L11 1 1650 270 5 -20 0 1 "249 nH" 1 "" 0>
  <Pac P5 1 1520 220 -34 -96 0 1 "5" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *12 5 1520 270 0 0 0 0>
  <GND *18 5 1750 300 0 0 0 0>
  <C C12 1 1750 270 15 -20 0 1 "2.13 pF" 1 "" 0 "neutral" 0>
  <L L12 1 1800 180 -26 10 0 0 "22.5 nH" 1 "" 0>
  <Pac P6 1 1870 210 -32 -87 0 1 "6" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *13 5 1870 260 0 0 0 0>
  <Pac P8 1 1880 480 -14 -87 0 1 "8" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *23 5 1880 530 0 0 0 0>
  <L L15 1 720 830 -26 10 0 0 "16.7 nH" 1 "" 0>
  <GND *21 5 780 950 0 0 0 0>
  <C C14 1 780 920 15 -20 0 1 "2.36 pF" 1 "" 0 "neutral" 0>
  <GND *24 5 1370 940 0 0 0 0>
  <RFEDD MAX2635 1 1320 830 -26 -40 0 0 "S" 0 "2" 0 "open" 0 "0.60408-j0.55918" 0 "0.0015072254+j0.0043615107" 0 "-2.8976231+j1.7137047" 0 "0.35996348-j0.17511106" 0>
  <L L16 1 1440 830 -28 -44 0 2 "22.5 nH" 1 "" 0>
  <Pac P9 1 610 870 18 -26 0 1 "9" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *28 5 610 950 0 0 0 0>
  <Pac P10 1 1540 860 18 -26 0 1 "10" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *29 5 1540 910 0 0 0 0>
  <C C15 1 1370 890 17 -26 0 1 "2.13 pF" 1 "" 0 "neutral" 0>
  <GND *30 4 1380 1470 0 0 0 0>
  <RFEDD MAX2636 2 1330 1360 -26 -40 0 0 "S" 0 "2" 0 "open" 0 "0.60408-j0.55918" 0 "0.0015072254+j0.0043615107" 0 "-2.8976231+j1.7137047" 0 "0.35996348-j0.17511106" 0>
  <C C17 1 730 1360 -20 -53 0 2 "100 pF" 1 "" 0 "neutral" 0>
  <Pac P11 1 640 1400 18 -26 0 1 "11" 1 "60 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *32 5 640 1480 0 0 0 0>
  <Pac P12 1 1530 1390 18 -26 0 1 "12" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *33 5 1530 1440 0 0 0 0>
  <Eqn Eqn12 1 60 1430 -31 15 0 0 "S1111dB=dB(S[11,11])" 1 "S1212dB=dB(S[12,12])" 1 "yes" 0>
  <Eqn Eqn13 1 60 1540 -31 15 0 0 "S1112B=dB(S[11,12])" 1 "S1211dB=dB(S[12,11])" 1 "yes" 0>
  <Eqn Eqn10 1 60 1210 -31 15 0 0 "S99dB=dB(S[9,9])" 1 "S1010dB=dB(S[10,10])" 1 "yes" 0>
  <Eqn Eqn11 1 60 1330 -31 15 0 0 "S910dB=dB(S[9,10])" 1 "S109dB=dB(S[10,9])" 1 "yes" 0>
  <Eqn Eqn2 1 50 720 -31 15 0 0 "S11dB=dB(S[1,1])" 1 "S22dB=dB(S[2,2])" 1 "yes" 0>
  <Eqn Eqn1 1 50 840 -31 15 0 0 "S12dB=dB(S[1,2])" 1 "S21dB=dB(S[2,1])" 1 "yes" 0>
  <Eqn Eqn3 1 60 980 -31 15 0 0 "S33dB=dB(S[3,3])" 1 "S44dB=dB(S[4,4])" 1 "yes" 0>
  <Eqn Eqn4 1 60 1090 -31 15 0 0 "S34dB=dB(S[3,4])" 1 "S43dB=dB(S[4,3])" 1 "yes" 0>
  <Eqn Eqn6 1 2120 740 -31 15 0 0 "S55dB=dB(S[5,5])" 1 "S66dB=dB(S[6,6])" 1 "yes" 0>
  <Eqn Eqn7 1 2120 860 -31 15 0 0 "S56dB=dB(S[5,6])" 1 "S65dB=dB(S[6,5])" 1 "yes" 0>
  <Eqn Eqn8 1 2120 990 -31 15 0 0 "S77dB=dB(S[7,7])" 1 "S88dB=dB(S[8,8])" 1 "yes" 0>
  <Eqn Eqn9 1 2120 1100 -31 15 0 0 "S78dB=dB(S[7,8])" 1 "S87dB=dB(S[8,7])" 1 "yes" 0>
  <SUBST Subst1 1 690 1100 -30 24 0 0 "4.6" 1 "0.21 mm" 1 "35 um" 1 "2e-4" 1 "0.022e-6" 1 "0.15e-6" 1>
  <MLIN MS1 1 1020 1080 -26 15 0 0 "Subst1" 1 "0.29 mm" 1 "2 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Pac P13 1 810 -270 18 -26 0 1 "13" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *34 5 810 -240 0 0 0 0>
  <L L24 1 960 -270 8 -26 0 1 "497 pH" 1 "" 0>
  <C C19 1 930 -270 -8 46 0 1 "318 pF" 1 "" 0 "neutral" 0>
  <GND *35 5 960 -240 0 0 0 0>
  <L L25 1 1070 -350 -26 -44 0 0 "1.59 uH" 1 "" 0>
  <C C20 1 1010 -350 -26 10 0 0 "99.5 fF" 1 "" 0 "neutral" 0>
  <L L26 1 1100 -270 8 -26 0 1 "497 pH" 1 "" 0>
  <C C21 1 1070 -270 -8 46 0 1 "318 pF" 1 "" 0 "neutral" 0>
  <GND *36 5 1100 -240 0 0 0 0>
  <Pac P14 1 1210 -270 18 -26 0 1 "14" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *37 5 1210 -240 0 0 0 0>
  <.SP SP1 1 2080 1270 0 70 0 0 "lin" 1 "200 MHz" 1 "500 MHz" 1 "1001" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <RFEDD TQP3M9038 1 830 830 -26 -40 0 0 "S" 0 "2" 0 "open" 0 "0.2785-j0.1647" 0 "0.0219+j0.02229" 0 "-9.74279+j14.39" 0 "0.2285+j0.07293" 0>
  <GND *20 5 890 950 0 0 0 0>
  <L L14 1 890 920 5 -20 0 1 "61.5 nH" 1 "" 0>
  <C C13 1 930 830 -26 17 0 0 "20.5 pF" 1 "" 0 "neutral" 0>
  <GND *25 5 1280 970 0 0 0 0>
  <L L17 1 1280 920 5 -20 0 1 "249 nH" 1 "" 0>
  <L L18 1 1240 830 -27 -45 0 2 "56.7 nH" 1 "" 0>
  <L L27 1 1020 910 8 -26 0 1 "497 pH" 1 "" 0>
  <GND *38 5 1020 940 0 0 0 0>
  <L L28 1 1130 830 -26 -44 0 0 "1.59 uH" 1 "" 0>
  <C C22 1 1070 830 -26 10 0 0 "99.5 fF" 1 "" 0 "neutral" 0>
  <L L29 1 1160 910 8 -26 0 1 "497 pH" 1 "" 0>
  <C C23 1 1130 910 -8 46 0 1 "318 pF" 1 "" 0 "neutral" 0>
  <GND *39 5 1160 940 0 0 0 0>
  <C C24 1 990 910 -8 46 0 1 "318 pF" 1 "" 0 "neutral" 0>
  <RFEDD TQP3M9039 1 800 1360 -26 -40 0 0 "S" 0 "2" 0 "open" 0 "0.2785-j0.1647" 0 "0.0219+j0.02229" 0 "-9.74279+j14.39" 0 "0.2285+j0.07293" 0>
  <GND *26 5 860 1480 0 0 0 0>
  <L L19 1 860 1450 5 -20 0 1 "68 nH" 1 "" 0>
  <C C16 1 910 1360 -26 17 0 0 "10 pF" 1 "" 0 "neutral" 0>
  <GND *31 4 1220 1490 0 0 0 0>
  <L L30 1 1010 1440 8 -26 0 1 "497 pH" 1 "" 0>
  <GND *40 5 1010 1470 0 0 0 0>
  <L L31 1 1120 1360 -26 -44 0 0 "1.59 uH" 1 "" 0>
  <C C25 1 1060 1360 -26 10 0 0 "99.5 fF" 1 "" 0 "neutral" 0>
  <L L32 1 1150 1440 8 -26 0 1 "497 pH" 1 "" 0>
  <C C26 1 1120 1440 -8 46 0 1 "318 pF" 1 "" 0 "neutral" 0>
  <GND *41 5 1150 1470 0 0 0 0>
  <C C27 1 980 1440 -8 46 0 1 "318 pF" 1 "" 0 "neutral" 0>
  <Eqn Eqn14 1 900 -170 -28 15 0 0 "S1413dB=dB(S[14,13])" 1 "S1313_dB=dB(S[13,13])" 1 "S1413phase=phase(S[14,13])" 1 "yes" 0>
  <L L13 1 1580 520 5 -20 0 1 "240 nH" 1 "" 0>
  <GND *19 5 1580 570 0 0 0 0>
  <C C6 1 570 450 -26 17 0 0 "10 pF" 1 "" 0 "neutral" 0>
  <L L21 0 1220 1440 5 -20 0 1 "240 nH" 1 "" 0>
  <L L22 2 1260 1360 -23 -43 0 2 "56 nH" 1 "" 0>
  <C C18 0 1380 1420 17 -26 0 1 "2 pF" 1 "" 0 "neutral" 0>
  <L L23 2 1450 1360 -18 -48 0 2 "20 nH" 1 "" 0>
</Components>
<Wires>
  <530 180 550 180 "" 0 0 0 "">
  <550 180 550 240 "" 0 0 0 "">
  <550 180 560 180 "" 0 0 0 "">
  <650 180 650 200 "" 0 0 0 "">
  <620 180 650 180 "" 0 0 0 "">
  <450 180 470 180 "" 0 0 0 "">
  <450 180 450 240 "" 0 0 0 "">
  <430 180 450 180 "" 0 0 0 "">
  <320 180 370 180 "" 0 0 0 "">
  <320 240 320 290 "" 0 0 0 "">
  <530 450 540 450 "" 0 0 0 "">
  <530 450 530 510 "" 0 0 0 "">
  <510 450 530 450 "" 0 0 0 "">
  <420 450 450 450 "" 0 0 0 "">
  <330 450 360 450 "" 0 0 0 "">
  <330 510 330 560 "" 0 0 0 "">
  <600 450 620 450 "" 0 0 0 "">
  <620 450 620 470 "" 0 0 0 "">
  <1750 450 1770 450 "" 0 0 0 "">
  <1770 450 1790 450 "" 0 0 0 "">
  <1770 450 1770 480 "" 0 0 0 "">
  <1770 540 1770 560 "" 0 0 0 "">
  <1660 450 1690 450 "" 0 0 0 "">
  <1500 450 1580 450 "" 0 0 0 "">
  <1500 510 1500 560 "" 0 0 0 "">
  <1620 180 1650 180 "" 0 0 0 "">
  <1650 180 1670 180 "" 0 0 0 "">
  <1650 180 1650 240 "" 0 0 0 "">
  <1520 180 1560 180 "" 0 0 0 "">
  <1520 180 1520 190 "" 0 0 0 "">
  <1520 250 1520 270 "" 0 0 0 "">
  <1730 180 1750 180 "" 0 0 0 "">
  <1750 180 1750 240 "" 0 0 0 "">
  <1750 180 1770 180 "" 0 0 0 "">
  <1830 180 1870 180 "" 0 0 0 "">
  <1870 240 1870 260 "" 0 0 0 "">
  <1850 450 1880 450 "" 0 0 0 "">
  <1880 510 1880 530 "" 0 0 0 "">
  <750 830 780 830 "" 0 0 0 "">
  <780 830 780 890 "" 0 0 0 "">
  <1350 830 1370 830 "" 0 0 0 "">
  <1370 830 1370 860 "" 0 0 0 "">
  <1370 920 1370 940 "" 0 0 0 "">
  <1370 830 1410 830 "" 0 0 0 "">
  <610 900 610 950 "" 0 0 0 "">
  <1540 890 1540 910 "" 0 0 0 "">
  <610 830 610 840 "" 0 0 0 "">
  <610 830 690 830 "" 0 0 0 "">
  <1470 830 1540 830 "" 0 0 0 "">
  <1360 1360 1380 1360 "" 0 0 0 "">
  <640 1430 640 1480 "" 0 0 0 "">
  <1530 1420 1530 1440 "" 0 0 0 "">
  <640 1360 640 1370 "" 0 0 0 "">
  <640 1360 700 1360 "" 0 0 0 "">
  <810 -350 810 -300 "" 0 0 0 "">
  <810 -350 960 -350 "" 0 0 0 "">
  <960 -350 960 -300 "" 0 0 0 "">
  <1100 -350 1100 -300 "" 0 0 0 "">
  <960 -350 980 -350 "" 0 0 0 "">
  <930 -300 960 -300 "" 0 0 0 "">
  <930 -240 960 -240 "" 0 0 0 "">
  <1070 -300 1100 -300 "" 0 0 0 "">
  <1070 -240 1100 -240 "" 0 0 0 "">
  <1210 -350 1210 -300 "" 0 0 0 "">
  <1100 -350 1210 -350 "" 0 0 0 "">
  <780 830 800 830 "" 0 0 0 "">
  <860 830 890 830 "" 0 0 0 "">
  <890 830 890 890 "" 0 0 0 "">
  <890 830 900 830 "" 0 0 0 "">
  <1280 950 1280 970 "" 0 0 0 "">
  <1280 830 1290 830 "" 0 0 0 "">
  <1280 830 1280 890 "" 0 0 0 "">
  <960 830 1020 830 "" 0 0 0 "">
  <1270 830 1280 830 "" 0 0 0 "">
  <1160 830 1210 830 "" 0 0 0 "">
  <1020 830 1040 830 "" 0 0 0 "">
  <1020 830 1020 880 "" 0 0 0 "">
  <1160 830 1160 880 "" 0 0 0 "">
  <990 880 1020 880 "" 0 0 0 "">
  <990 940 1020 940 "" 0 0 0 "">
  <1130 880 1160 880 "" 0 0 0 "">
  <1130 940 1160 940 "" 0 0 0 "">
  <830 1360 870 1360 "" 0 0 0 "">
  <760 1360 770 1360 "" 0 0 0 "">
  <860 1420 870 1420 "" 0 0 0 "">
  <870 1360 870 1420 "" 0 0 0 "">
  <940 1360 1010 1360 "" 0 0 0 "">
  <870 1360 880 1360 "" 0 0 0 "">
  <1150 1360 1220 1360 "" 0 0 0 "">
  <1010 1360 1030 1360 "" 0 0 0 "">
  <1010 1360 1010 1410 "" 0 0 0 "">
  <1150 1360 1150 1410 "" 0 0 0 "">
  <980 1410 1010 1410 "" 0 0 0 "">
  <980 1470 1010 1470 "" 0 0 0 "">
  <1120 1410 1150 1410 "" 0 0 0 "">
  <1120 1470 1150 1470 "" 0 0 0 "">
  <1580 550 1580 570 "" 0 0 0 "">
  <1580 450 1600 450 "" 0 0 0 "">
  <1580 450 1580 490 "" 0 0 0 "">
  <1220 1360 1220 1410 "" 0 0 0 "">
  <1220 1470 1220 1490 "" 0 0 0 "">
  <1220 1360 1230 1360 "" 0 0 0 "">
  <1290 1360 1300 1360 "" 0 0 0 "">
  <1380 1360 1380 1390 "" 0 0 0 "">
  <1380 1450 1380 1470 "" 0 0 0 "">
  <1380 1360 1420 1360 "" 0 0 0 "">
  <1480 1360 1530 1360 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 760 590 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -69.4352 20 -11.9867 1 -30 10 -4.8536 315 0 225 "" "" "" "">
	<"S44dB" #ff0000 2 3 0 0 0>
  </Rect>
  <Rect 1140 320 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -67.0687 20 -13.8654 1 -17.4751 5 -9.46268 315 0 225 "" "" "" "">
	<"S55dB" #0000ff 2 3 0 0 0>
  </Rect>
  <Rect 1140 590 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -15 5 -4.21897 1 -1 1 1 315 0 225 "" "" "" "">
	<"S77dB" #ff0000 2 3 0 0 0>
	  <Mkr 4.007e+08 221 -79 3 0 0>
  </Rect>
  <Rect 2050 310 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -69.4352 20 -11.9867 1 -30 10 -4.8536 315 0 225 "" "" "" "">
	<"S66dB" #0000ff 2 3 0 0 0>
  </Rect>
  <Rect 2050 590 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -60 20 -6.18654 1 -1 1 1 315 0 225 "" "" "" "">
	<"S88dB" #ff0000 2 3 0 0 0>
	  <Mkr 3.983e+08 219 -162 3 0 0>
  </Rect>
  <Rect 270 870 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -20 5 -9.34169 1 -1 1 1 315 0 225 "" "" "" "">
	<"S99dB" #0000ff 2 3 0 0 0>
  </Rect>
  <Rect 260 1110 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -49.5605 20 0 1 -1 1 1 315 0 225 "" "" "" "">
	<"S910dB" #0000ff 2 3 0 0 0>
  </Rect>
  <Rect 270 1388 302 168 3 #c0c0c0 1 00 0 2e+08 5e+07 5e+08 0 -20 5 0 1 -1 1 1 315 0 225 "" "" "" "">
	<"S1111dB" #ff0000 2 3 0 0 0>
  </Rect>
  <Rect 260 1620 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -76.6642 2 -70.7239 1 -1 1 1 315 0 225 "" "" "" "">
	<"S1112B" #55aa00 2 3 0 0 0>
  </Rect>
  <Rect 1670 1378 314 168 3 #c0c0c0 1 00 1 2e+08 5e+07 5e+08 0 -40 5 -5 1 -1 1 1 315 0 225 "" "" "" "">
	<"S1212dB" #ff0000 2 3 0 0 0>
  </Rect>
  <Rect 1670 1600 322 150 3 #c0c0c0 1 00 1 2e+08 5e+07 5e+08 0 -20 10 50 1 -1 1 1 315 0 225 "" "" "" "">
	<"S1211dB" #55aa00 2 3 0 0 0>
  </Rect>
  <Rect 1670 850 343 170 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -67.0227 20 -6.24696 1 -1 1 1 315 0 225 "" "" "" "">
	<"S1010dB" #0000ff 2 3 0 0 0>
  </Rect>
  <Rect 1670 1090 341 170 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 0 -30 10 50 1 -1 1 1 315 0 225 "" "" "" "">
	<"S109dB" #0000ff 2 3 0 0 0>
	  <Mkr 3.995e+08 237 -212 3 0 0>
  </Rect>
  <Rect 130 -125 637 225 3 #c0c0c0 1 00 0 3.8e+08 5e+06 4.2e+08 0 -50 5 5 1 -1 0.5 1 315 0 225 "" "" "" "">
	<"S1313_dB" #0000ff 2 3 0 0 0>
  </Rect>
  <Rect 1470 -135 763 235 3 #c0c0c0 1 00 0 3.8e+08 5e+06 4.2e+08 0 -20 5 5 1 -1 0.5 1 315 0 225 "" "" "" "">
	<"S1413dB" #0000ff 2 3 0 0 0>
	  <Mkr 4.001e+08 453 -288 3 0 0>
	<"S1413phase" #ff0000 2 3 0 0 1>
  </Rect>
  <Rect 1 569 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 0 -20 2.5 -10 1 -1 1 1 315 0 225 "" "" "" "">
	<"S33dB" #ff0000 2 3 0 0 0>
  </Rect>
  <Rect 761 329 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -69.4352 20 -11.9867 1 -30 10 -4.8536 315 0 225 "" "" "" "">
	<"S22dB" #0000ff 0 3 0 0 0>
  </Rect>
  <Rect 1 319 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -67.0687 20 -13.8654 1 -17.4751 5 -9.46268 315 0 225 "" "" "" "">
	<"S11dB" #0000ff 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Text 360 360 16 #000000 0 "Experimentally used values\n">
  <Text 1620 370 16 #000000 0 "Experimentally used values\n">
  <Text 1540 100 16 #000000 0 "Theoretically calculated values\n">
  <Text 350 100 16 #000000 0 "Theoretically calculated values\n">
  <Text 370 50 16 #ff0000 0 "First amplification stage\n">
  <Text 1560 60 16 #ff0000 0 "Second amplification stage\n">
  <Text 1100 -150 12 #000000 0 "Butterworth band-pass filter \n 395 MHz...405 MHz, pi-type, \n impedance matching 50 Ohm">
  <Rectangle 960 780 250 240 #aa00ff 2 2 #c0c0c0 1 0>
  <Text 940 720 16 #000000 0 "2 stage amplifier chain\nTheoretically calculated values">
  <Rectangle 950 1310 250 240 #aa00ff 2 2 #c0c0c0 1 0>
  <Text 930 1250 16 #000000 0 "2 stage amplifier chain\nExperimentally used values">
  <Text 630 1040 12 #aa0000 0 "JLC04161H-7628 Stackup">
</Paintings>
