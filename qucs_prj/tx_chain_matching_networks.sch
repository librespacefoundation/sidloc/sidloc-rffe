<Qucs Schematic 24.3.0>
<Properties>
  <View=-277,-396,2736,2979,0.667017,236,810>
  <Grid=10,10,1>
  <DataSet=tx_chain_matching_networks.dat>
  <DataDisplay=tx_chain_matching_networks.dpl>
  <OpenDisplay=0>
  <Script=BGA6130b.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <MVIA MS24 1 480 410 20 0 0 0 "Subst2" 1 "0.3 mm" 1 "26.85" 0>
  <SPfile X1 1 1200 290 -26 -59 0 0 "./BGA6130.s2p" 0 "polar" 0 "linear" 0 "open" 0 "2" 0>
  <MVIA MS28 1 1710 400 20 0 0 0 "Subst2" 1 "0.3 mm" 1 "26.85" 0>
  <C C6 1 1900 290 -20 -53 0 2 "1nF" 1 "" 0 "neutral" 0>
  <GND * 5 1200 400 0 0 0 0>
  <MLIN MS25 1 1290 290 -17 -91 0 0 "Subst2" 1 "0.16 mm" 1 "1.4 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS26 1 1450 290 -17 -91 0 0 "Subst2" 1 "0.16 mm" 1 "1.4 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS27 1 1640 290 -17 -91 0 0 "Subst2" 1 "0.16 mm" 1 "1.3 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS29 1 1790 290 -17 -91 0 0 "Subst2" 1 "0.16 mm" 1 "3.2 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS23 1 1080 290 -17 -91 0 0 "Subst2" 1 "0.16 mm" 1 "1.4 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS22 1 750 290 -17 -91 0 0 "Subst2" 1 "0.16 mm" 1 "1 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS21 1 520 290 -17 -91 0 0 "Subst2" 1 "0.16 mm" 1 "1 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <C C19 1 280 290 -20 -53 0 2 "220 pF" 1 "" 0 "neutral" 0>
  <L L3 1 1550 220 10 -26 0 1 "68nH" 1 "" 0>
  <GND *6 5 1550 170 0 0 0 2>
  <C C3 1 920 260 -20 -53 0 2 "15pF" 1 "" 0 "neutral" 0>
  <R R1 1 920 320 -26 15 0 0 "120 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <SUBST Subst2 1 1790 -10 -30 24 0 0 "4.1" 1 "0.0994 mm" 1 "35 um" 1 "2e-4" 1 "0.022e-6" 1 "0.15e-6" 1>
  <Eqn Eqn1 1 1990 -40 -31 15 0 0 "S11dB=dB(S[1,1])" 1 "S22dB=dB(S[2,2])" 1 "yes" 0>
  <Eqn Eqn2 1 1990 80 -31 15 0 0 "S12dB=dB(S[1,2])" 1 "S21dB=dB(S[2,1])" 1 "yes" 0>
  <GND *7 5 900 1730 0 0 0 0>
  <R R2 1 810 1700 -26 15 0 0 "120 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <C C15 1 580 1660 -20 -53 0 2 "1nF" 1 "" 0 "neutral" 0>
  <SPfile X2 1 900 1660 -26 -59 0 0 "./BGA6130.s2p" 1 "polar" 0 "linear" 0 "open" 0 "2" 0>
  <GND *8 5 690 1770 0 0 0 0>
  <MLIN MS16 1 650 1660 -17 -91 0 0 "Subst1" 1 "0.35 mm" 1 "2 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS17 1 480 1660 -17 -91 0 0 "Subst1" 1 "0.35 mm" 1 "2.1 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <C C8 1 810 1640 -20 -53 0 2 "15 pF" 1 "" 0 "neutral" 0>
  <C C9 1 690 1720 17 -12 0 1 "15pF" 1 "" 0 "neutral" 0>
  <L L6 1 740 1660 -26 10 0 0 "20nH" 1 "" 0>
  <MLIN MS13 1 1700 1660 -78 -110 0 0 "Subst1" 1 "0.35 mm" 1 "1.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS12 1 1570 1660 -40 26 0 0 "Subst1" 1 "0.35 mm" 1 "1.52 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <L L8 1 1640 1660 -26 10 0 0 "8.2 nH" 1 "" 0>
  <SPfile X3 1 1500 1660 -26 -59 0 0 "./BGA6130.s2p" 1 "polar" 0 "linear" 0 "open" 0 "2" 0>
  <C C11 1 2040 1660 -20 -53 0 2 "1nF" 1 "" 0 "neutral" 0>
  <GND *16 5 2190 1770 0 0 0 0>
  <Pac P4 1 2190 1710 26 -23 0 1 "4" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "true" 0>
  <MLIN MS14 1 1970 1660 -62 -77 0 0 "Subst1" 1 "0.35 mm" 1 "3.5 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <GND *23 5 1790 1560 0 0 0 2>
  <L L11 1 1790 1630 10 -26 0 1 "68nH" 1 "" 0>
  <GND *11 5 1500 1730 0 0 0 0>
  <GND *15 5 350 1760 0 0 0 0>
  <Pac P3 1 350 1710 -108 -15 0 1 "3" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "true" 0>
  <MLIN MS18 1 1020 1660 -40 26 0 0 "Subst1" 1 "0.35 mm" 1 "4.3 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <C C16 1 1110 1660 -30 -56 0 0 "470 pF" 1 "" 0 "neutral" 0>
  <L L12 1 960 1580 10 -26 0 1 "9.1nH" 1 "" 0>
  <GND *14 5 1880 1840 0 0 0 0>
  <GND * 4 1400 1860 0 0 0 0>
  <C C14 1 1880 1750 15 -20 0 1 "15 pF" 1 "" 0 "neutral" 0>
  <SPfile X4 1 2130 1660 -26 -59 0 0 "LFCG-490+_Plus25degC.s2p" 1 "polar" 0 "linear" 0 "open" 0 "2" 0>
  <GND * 5 2130 1770 0 0 0 0>
  <Pac P5 0 1163 1791 -108 -15 0 1 "5" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "true" 0>
  <Pac P6 0 1403 1791 -108 -15 0 1 "6" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "true" 0>
  <GND * 4 1160 1860 0 0 0 0>
  <MLIN MS19 1 1280 1660 -40 26 0 0 "Subst1" 1 "0.35 mm" 1 "4.2 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Eqn Eqn3 1 330 2030 -31 15 0 0 "S33dB=dB(S[3,3])" 1 "S44dB=dB(S[4,4])" 1 "S55dB=dB(S[5,5])" 1 "yes" 0>
  <Eqn Eqn4 1 330 2140 -31 15 0 0 "S34dB=dB(S[3,4])" 1 "S43dB=dB(S[4,3])" 1 "S45dB=dB(S[4,5])" 1 "yes" 0>
  <SUBST Subst1 1 160 2070 -30 24 0 0 "4.6" 1 "0.21 mm" 1 "35 um" 1 "2e-4" 1 "0.022e-6" 1 "0.15e-6" 1>
  <Attenuator X5 1 400 1660 -26 21 0 0 "5 dB" 1 "50 Ohm" 0 "26.85" 0>
  <GND *24 5 960 1490 0 0 0 2>
  <MLIN MS20 1 380 290 -17 -91 0 0 "Subst2" 1 "0.16 mm" 1 "4.1 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Pac P2 1 2010 330 64 -25 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "true" 0>
  <GND *2 5 2010 400 0 0 0 0>
  <Pac P1 1 160 340 -108 -15 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "true" 0>
  <GND *1 5 160 420 0 0 0 0>
  <L L1 1 640 290 -26 10 0 0 "18nH" 1 "" 0>
  <C C2 1 460 350 17 -12 0 1 "20pF" 1 "" 0 "neutral" 0>
  <C C5 1 1690 350 15 -20 0 1 "18 pF" 1 "" 0 "neutral" 0>
  <L L2 1 1370 290 -26 10 0 0 "8.2 nH" 1 "" 0>
  <.SP SP1 1 150 -230 0 61 0 0 "lin" 1 "200 MHz" 1 "500 MHz" 1 "1001" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <C C20 0 870 -190 -20 -53 0 2 "220 pF" 1 "" 0 "neutral" 0>
  <L L13 0 810 -260 10 -26 0 1 "68nH" 1 "" 0>
  <Attenuator X10 0 560 -190 -26 21 0 0 "10 dB" 1 "50 Ohm" 0 "26.85" 0>
  <C C21 0 650 -190 -20 -53 0 2 "220 pF" 1 "" 0 "neutral" 0>
  <SPfile X11 0 740 -190 -26 -42 0 0 "QPA4263C_De-embedded_SPARAM.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <GND * 0 740 -70 0 0 0 0>
  <SPfile X12 0 1190 -190 -26 -59 0 0 "LFCG-490+_Plus25degC.s2p" 1 "polar" 0 "linear" 0 "open" 0 "2" 0>
  <GND *28 4 1190 -160 0 0 0 0>
  <GND *27 4 810 -340 0 0 0 2>
</Components>
<Wires>
  <460 290 460 320 "" 0 0 0 "">
  <460 380 460 410 "" 0 0 0 "">
  <310 290 350 290 "" 0 0 0 "">
  <410 290 460 290 "" 0 0 0 "">
  <460 290 490 290 "" 0 0 0 "">
  <1110 290 1170 290 "" 0 0 0 "">
  <1230 290 1260 290 "" 0 0 0 "">
  <1690 290 1690 320 "" 0 0 0 "">
  <1690 380 1690 400 "" 0 0 0 "">
  <1670 290 1690 290 "" 0 0 0 "">
  <1690 290 1760 290 "" 0 0 0 "">
  <1820 290 1870 290 "" 0 0 0 "">
  <1200 320 1200 400 "" 0 0 0 "">
  <670 290 720 290 "" 0 0 0 "">
  <550 290 610 290 "" 0 0 0 "">
  <1400 290 1420 290 "" 0 0 0 "">
  <1320 290 1340 290 "" 0 0 0 "">
  <1480 290 1550 290 "" 0 0 0 "">
  <1550 290 1610 290 "" 0 0 0 "">
  <1550 250 1550 290 "" 0 0 0 "">
  <1550 170 1550 190 "" 0 0 0 "">
  <970 290 1050 290 "" 0 0 0 "">
  <780 290 870 290 "" 0 0 0 "">
  <870 290 870 320 "" 0 0 0 "">
  <870 320 890 320 "" 0 0 0 "">
  <870 260 870 290 "" 0 0 0 "">
  <870 260 890 260 "" 0 0 0 "">
  <950 260 970 260 "" 0 0 0 "">
  <970 260 970 290 "" 0 0 0 "">
  <950 320 970 320 "" 0 0 0 "">
  <970 290 970 320 "" 0 0 0 "">
  <900 1690 900 1730 "" 0 0 0 "">
  <840 1660 840 1700 "" 0 0 0 "">
  <840 1660 870 1660 "" 0 0 0 "">
  <780 1660 780 1700 "" 0 0 0 "">
  <610 1660 620 1660 "" 0 0 0 "">
  <770 1660 780 1660 "" 0 0 0 "">
  <510 1660 550 1660 "" 0 0 0 "">
  <780 1640 780 1660 "" 0 0 0 "">
  <840 1640 840 1660 "" 0 0 0 "">
  <680 1660 690 1660 "" 0 0 0 "">
  <690 1660 710 1660 "" 0 0 0 "">
  <690 1660 690 1690 "" 0 0 0 "">
  <690 1750 690 1770 "" 0 0 0 "">
  <1600 1660 1610 1660 "" 0 0 0 "">
  <1530 1660 1540 1660 "" 0 0 0 "">
  <2000 1660 2010 1660 "" 0 0 0 "">
  <2190 1740 2190 1770 "" 0 0 0 "">
  <1730 1660 1790 1660 "" 0 0 0 "">
  <1790 1560 1790 1600 "" 0 0 0 "">
  <1500 1690 1500 1730 "" 0 0 0 "">
  <350 1740 350 1760 "" 0 0 0 "">
  <1050 1660 1080 1660 "" 0 0 0 "">
  <1140 1660 1160 1660 "" 0 0 0 "">
  <930 1660 960 1660 "" 0 0 0 "">
  <960 1660 990 1660 "" 0 0 0 "">
  <960 1610 960 1660 "" 0 0 0 "">
  <1790 1660 1880 1660 "" 0 0 0 "">
  <1880 1660 1940 1660 "" 0 0 0 "">
  <1880 1660 1880 1720 "" 0 0 0 "">
  <1880 1780 1880 1840 "" 0 0 0 "">
  <1400 1660 1470 1660 "" 0 0 0 "">
  <2190 1660 2190 1680 "" 0 0 0 "">
  <2160 1660 2190 1660 "" 0 0 0 "">
  <2070 1660 2100 1660 "" 0 0 0 "">
  <2130 1690 2130 1770 "" 0 0 0 "">
  <1400 1821 1403 1821 "" 0 0 0 "">
  <1400 1821 1400 1860 "" 0 0 0 "">
  <1400 1761 1403 1761 "" 0 0 0 "">
  <1400 1660 1400 1761 "" 0 0 0 "">
  <1160 1761 1163 1761 "" 0 0 0 "">
  <1160 1660 1160 1761 "" 0 0 0 "">
  <1160 1821 1163 1821 "" 0 0 0 "">
  <1160 1821 1160 1860 "" 0 0 0 "">
  <1310 1660 1400 1660 "" 0 0 0 "">
  <1160 1660 1250 1660 "" 0 0 0 "">
  <430 1660 450 1660 "" 0 0 0 "">
  <350 1660 350 1680 "" 0 0 0 "">
  <350 1660 370 1660 "" 0 0 0 "">
  <960 1490 960 1550 "" 0 0 0 "">
  <1930 290 2010 290 "" 0 0 0 "">
  <2010 290 2010 300 "" 0 0 0 "">
  <2010 360 2010 400 "" 0 0 0 "">
  <160 290 250 290 "" 0 0 0 "">
  <160 290 160 310 "" 0 0 0 "">
  <160 370 160 420 "" 0 0 0 "">
  <590 -190 620 -190 "" 0 0 0 "">
  <680 -190 710 -190 "" 0 0 0 "">
  <770 -190 810 -190 "" 0 0 0 "">
  <810 -190 840 -190 "" 0 0 0 "">
  <810 -230 810 -190 "" 0 0 0 "">
  <740 -160 740 -70 "" 0 0 0 "">
  <810 -340 810 -290 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 110 1223 815 333 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 0 5 5 20 1 -1 1 1 315 0 225 1 0 0 "" "" "">
	<"S21dB" #0000ff 2 3 0 0 0>
	  <Mkr 3.995e+08 222 -312 3 0 0>
  </Rect>
  <Smith 2201 2228 497 497 3 #c0c0c0 1 00 1 0 1 1 1 0 4 1.21519 1 0 1 1 315 0 225 1 0 0 "" "" "">
	<"S[3,3]" #aa0000 2 3 0 0 0>
	  <Mkr 4.016e+08 326 -415 3 0 0>
	<"S[4,4]" #0000ff 2 3 0 0 0>
	  <Mkr 4.025e+08 -31 -385 3 0 0>
  </Smith>
  <Rect 480 2230 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -21.9438 10 1.41193 1 -1 1 1 315 0 225 1 0 0 "" "" "">
	<"S33dB" #ff0000 2 3 0 0 0>
	  <Mkr 4.01e+08 255 96 3 0 0>
  </Rect>
  <Rect 1420 2230 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -23.9321 10 1.65314 1 -1 1 1 315 0 225 1 0 0 "" "" "">
	<"S44dB" #ff0000 2 3 0 0 0>
	  <Mkr 4.01e+08 138 156 3 0 0>
	  <Mkr 3.683e+08 -105 105 3 0 0>
  </Rect>
  <Rect 790 2230 240 160 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -23.9321 10 1.65314 1 -1 1 1 315 0 225 1 0 0 "" "" "">
	<"S34dB" #0000ff 2 3 0 0 0>
  </Rect>
  <Smith 1870 2779 519 519 3 #c0c0c0 1 00 1 0 1 1 1 0 4 1.08571 1 0 1 1 315 0 225 1 0 0 "" "" "">
	<"S[5,5]" #0000ff 2 3 0 0 0>
	  <Mkr 3.998e+08/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0 142 -165 3 0 0>
	<"S[6,6]" #ff0000 2 3 0 0 0>
	  <Mkr 4.01e+08/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0 -191 -209 3 0 0>
  </Smith>
  <Rect 840 2883 736 423 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 0 0 5 40 1 -1 1 1 315 0 225 1 0 0 "" "" "">
	<"S43dB" #ff0000 2 3 0 0 0>
	  <Mkr 3.908e+08 298 -400 3 0 0>
	  <Mkr 4.1e+08 575 -401 3 0 0>
  </Rect>
  <Smith 1591 1209 628 628 3 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 1 1 315 0 225 1 0 0 "" "" "">
	<"S[1,1]" #0000ff 2 3 0 0 0>
	  <Mkr 4.001e+08 356 -556 3 0 0>
	<"S[2,2]" #ff0000 2 3 0 0 0>
	  <Mkr 4.001e+08 -119 -529 3 0 0>
  </Smith>
  <Rect 1020 1229 472 299 3 #c0c0c0 1 00 1 2e+08 5e+07 5e+08 1 -20.854 5 0 1 -1 0.5 1 315 0 225 1 0 0 "" "" "">
	<"S22dB" #0000ff 2 3 0 0 0>
	  <Mkr 4.007e+08 326 -284 3 0 0>
  </Rect>
  <Rect 790 835 622 295 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -15.4992 10 3.77993 1 -1 1 1 315 0 225 1 0 0 "" "" "">
	<"S11dB" #0000ff 2 3 0 0 0>
	  <Mkr 4.022e+08 189 -106 3 0 0>
  </Rect>
  <Rect 120 842 576 302 3 #c0c0c0 1 00 1 2e+08 1e+08 5e+08 1 -41.925 5 -29.1588 1 -1 1 1 315 0 225 1 0 0 "" "" "">
	<"S12dB" #0000ff 2 3 0 0 0>
	  <Mkr 4.028e+08 132 -300 3 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Text 1760 -90 16 #ff0000 0 "JLC04161H-3313 Stackup">
  <Text 130 1970 16 #ff0000 0 "JLC04161H-7628 Stackup">
  <Text 70 1440 16 #ff0000 0 " Double stage BGA6130 with\noptiized interstage matching network">
  <Text 50 60 16 #ff0000 0 " Single stage BGA6130 Matching networks">
</Paintings>
